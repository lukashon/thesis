# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import torch
from torch.utils.data import Dataset
from utils.preproc import add_missing_values

np.random.seed(8)

class DatasetM(Dataset):
    
    def __init__(self, file_path):
        
        label = file_path.split('_')
        self.label = int(label[-2])
        self.data = pd.read_csv(file_path).values
        
        
        
    def add_missing(self, stride=1, sparcity=0.02, fill_method='zero'):
        
        self.with_missing = add_missing_values(self.data,
                                         stride = stride,
                                         sparcity = sparcity,
                                         fill_method = fill_method)
        
    
    def stack_with_indicator(self):
        
        self.data_and_mask = torch.as_tensor(
                            np.stack(self.with_missing, axis=2)
                            )


        return self.data_and_mask, self.label #tensor(window, features, 2), int
    
    

    
    