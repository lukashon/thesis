# -*- coding: utf-8 -*-
import matplotlib.pyplot as plt
import glob
import pandas as pd
import numpy as np

def create_training_plots(output_path,
                       train_loss,
                       test_loss,
                       train_acc,
                       test_acc,
                       tag='Validation'):

    fig, ax =  plt.subplots(1, 2, figsize = (16, 5))
    
    ax[0].plot(range(len(train_loss)), train_loss, label = 'Training loss')
    ax[0].plot(range(len(test_loss)), test_loss, label = f'{tag} loss')
    ax[0].legend()
    ax[0].set(xlabel='Epochs', ylabel='Loss')

    ax[1].plot(range(len(train_acc)), train_acc, label = 'Training accuracy')
    ax[1].plot(range(len(test_acc)), test_acc, label = f'{tag} accuracy')
    ax[1].legend()
    ax[1].set(xlabel='Epochs', ylabel='Accuracy')
    
    plt.savefig(output_path+'/plots.png')
    plt.close()
    
    return
    
def create_run_comparison(run_folder):
    
    files = glob.glob(run_folder+'*/summary.csv')

    df = pd.concat([pd.read_csv(file, sep = '\t', index_col=0).T for file in files])
    df.drop('network', axis = 1, inplace = True)
    df.reset_index(drop=True, inplace=True)
    

    for c in ['test_accuracy', 'sparcity']:
        df[c] = df[c].astype(float)
        
    
    y_min = min(df['test_accuracy'])*0.9
    
    fig, ax =  plt.subplots(2, 2, figsize = (16, 8))
    
    df1 = df[df['fill_method'] == 'zero']
    df1_i = df1[df1['indicator_added'] == 'True']
    df1_i.sort_values('sparcity', inplace = True)
    
        
    ax[0,0].bar(np.arange(len(df1_i)), df1_i['test_accuracy'].values)
    ax[0,0].set(xticklabels = df1_i['sparcity'].values, 
                  xlabel='Sparcity', 
                  ylabel='Test accuracy, %',
                  title = 'Zero fill, with missingness indicator',
                  ylim = (y_min, 100))
    
    
    df1_ni = df1[df1['indicator_added'] == 'False']
    df1_ni.sort_values('sparcity', inplace = True)
    
    ax[0,1].bar(np.arange(len(df1_ni)), df1_ni['test_accuracy'].values, )
    ax[0,1].set(xticklabels = df1_ni['sparcity'].values, 
                  xlabel='Sparcity', 
                  ylabel='Test accuracy, %',
                  title = 'Zero fill, no missingness indicator',
                  ylim = (y_min, 100))
    
    
    
    
    df2 = df[df['fill_method'] == 'forw_fill']
    
    df2_i = df2[df2['indicator_added']  == 'True']
    df2_i.sort_values('sparcity', inplace = True)
    
    ax[1,0].bar(np.arange(len(df2_i)), df2_i['test_accuracy'].values)
    ax[1,0].set(xticklabels = df2_i['sparcity'].values, 
                  xlabel='Sparcity', 
                  ylabel='Test accuracy, %',
                  title = 'Forward fill, with missingness indicator',
                  ylim = (y_min, 100))
    
    
    df2_ni = df2[df2['indicator_added']  == 'False']
    df2_ni.sort_values('sparcity', inplace = True)
    
    ax[1,1].bar(np.arange(len(df2_ni)), df2_ni['test_accuracy'].values)
    ax[1,1].set(xticklabels = df2_ni['sparcity'].values, 
                  xlabel='Sparcity', 
                  ylabel='Test accuracy, %',
                  title = 'Forward fill, no missingness indicator',
                  ylim = (y_min, 100))
   

    
