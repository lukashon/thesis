# -*- coding: utf-8 -*-
from glob import glob
import numpy as np

from torch.utils.data import DataLoader
import torch

from utils.dataset import DatasetM
from utils.preproc import create_train_test_split

np.random.seed(8)

def create_dataloaders(config, 
                       add_missing_indicator=True,
                       sparcity=0.02,
                       fill_method="zero",
                       recode=False):

    files = glob(config['data_dir']+'*.csv')
    
    data = []
    labels = []
    
    for file in files:
        item = DatasetM(file)
        item.add_missing(stride=config['miss_stride'],
                                         sparcity=sparcity,
                                         fill_method=fill_method
                                                 )
        
        if add_missing_indicator:
            datapoint, label = item.stack_with_indicator()
            
        else:
            datapoint, label = torch.as_tensor(item.data), item.label
            datapoint = torch.unsqueeze(datapoint, -1)
          
        data.append(datapoint)
        labels.append(label)
    
    if recode==True:
        pamap2 = dict(zip([1,2,3,4,12,13,16,17], 
                          range(8)
                          ))
        labels = [pamap2[L] for L in labels]
                                     
    
    
    train, validation, test = create_train_test_split(labels, 
                                                      config['test_share'], 
                                                      config['validation_share']
                                                      )
    
    train_set = list(zip([data[i] for i in train], 
                    [labels[i] for i in train])
                    )
    
    val_set = list(zip([data[i] for i in validation], 
                    [labels[i] for i in validation])
                    )
    
    test_set = list(zip([data[i] for i in test], 
                    [labels[i] for i in test])
                    )
    
    
    trainloader = DataLoader(train_set, batch_size=config['batch'],
                             shuffle=True, num_workers=config['num_workers']
                             )
    
    valloader = DataLoader(val_set, batch_size=config['batch'],
                             shuffle=False, num_workers=config['num_workers']
                            )
    
    testloader = DataLoader(test_set, batch_size=config['batch'],
                            shuffle=False, num_workers=config['num_workers']
                            )
    
    return trainloader, valloader, testloader
    
    