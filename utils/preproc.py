# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import os
from random import shuffle 

np.random.seed(8)

def sliding_window(df,L,P): #L-sliding window lenght, P-sliding step
    i=0
    m=0
    s=pd.DataFrame()
    k=0
    while k < (df.shape[0]):
        sub=df.iloc[i:(i+L),:]
        s=s.append(sub)
        i+=P
        m+=1
        k=i+L
    return s,m #s - df of subsequences, m-number of subsequences

def adjust_names(folder, split):
    
    files = os.listdir(folder)
    files = [f for f in files  if f.endswith('.csv')]
    
    train, validation, test = split[0], split[1], split[2]
    
    for filename in files:
        filename_parced = filename.split('_')
        idx = int(filename_parced[-1].replace('.csv', ''))
        if idx in train:
            os.rename(folder+filename, folder+'train_'+ filename)
        if idx in validation:
            os.rename(folder+filename, folder+'validation_'+ filename)
        if idx in test:
            os.rename(folder+filename, folder+'test_'+ filename)
  
    return

def create_train_test_split(labels, test_share, validation_share):
    
    train = []
    test = []
    validation = []
    
    for label in set(labels):
       indices = [i for i, j in enumerate(labels) if j == label]
       shuffle(indices)
       len_test  = int(len(indices)*test_share)
       len_val = int(len(indices)*validation_share)
       
       test.extend(indices[:len_test])
       validation.extend(indices[len_test:len_test+len_val])
       train.extend(indices[len_test+len_val:])
   
    return train, validation, test


def standardize_dataset(df, class_col):
    
    clean=df.copy()
    clean.drop(class_col, inplace = True, axis=1)
    stdev=np.nanstd(clean, axis=0)
    mean=np.nanmean(clean, axis=0)
    clean=(clean-mean)/stdev 
    clean[class_col]=df[class_col]
    df = clean
    
    return df
    


def add_missing_values(data, stride=1, sparcity = 0.02, fill_method = 'zero'):
    
    shape = data.shape
    size = shape[0]*shape[1]
    
    mask = np.zeros(size, dtype=int)
    mask[:int(size*sparcity/stride)] = 1
    np.random.shuffle(mask)
    
    mask = mask.reshape(shape)
    mask_orig = mask.copy()
    for step in range(1,stride):
        mask = mask + np.hstack((np.zeros((shape[0], step)), mask_orig[:, :-step]))
        
    mx = np.ma.masked_array(data, mask=mask)
    
    if fill_method == 'zero':
        mx = mx.filled(0)
    if fill_method == 'forw_fill':
        
        df = pd.DataFrame(mx)
        df.fillna(method='ffill', axis=1, inplace=True)
        df.fillna(method='bfill', axis=1, inplace=True)
        mx = df.to_numpy()
    if fill_method == 'nan':
        mx = mx.filled(np.nan)
    
    return mx, mask