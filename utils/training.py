# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter

from utils.vis import create_training_plots


np.random.seed(8)



def train_network(config, 
                  net, 
                  train_loader,
                  val_loader, 
                  missing_values,
                  output_path,
#                  switch_dimensions,
                  classes):
    
    net = net.double() 
    criterion = nn.CrossEntropyLoss()
    optimizer = optim.SGD(net.parameters(), 
                          momentum=config['momentum'],
                          lr=config['learning_rate'], 
                          weight_decay=config['decay']
                          )
    


    writer = SummaryWriter(output_path+'tb/')
    
    train_losses = []
    val_losses = []
    train_accs = []
    val_accs = []
    
    for epoch in range(config['epochs']):  

        running_loss = 0
        running_val_loss = 0
        
        correct = 0
        total = 0
        
        correct_val = 0
        total_val = 0
        
        for data in train_loader:
            # get the inputs
            inputs, labels = data[0], torch.as_tensor(data[1]).long()
            
            inputs = torch.transpose(inputs, 1,3).double()  #(n_samples, channels, features, window_length)
#            inputs = torch.transpose(inputs, 1, 3).double()  #(n_samples, channels, features, window_length)

#            print(torch.max(inputs), torch.min(inputs))
            #zero the parameter gradients
            optimizer.zero_grad()

            #forward + backward + optimize
            outputs = net(inputs)
            
#            print(outputs)
            
            loss = criterion(outputs,labels)
            loss.backward()
            optimizer.step()

            with torch.no_grad():
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                correct += (predicted == labels).sum().item()

            running_loss += loss.item()
            
            train_acc=100*correct/total
            
        with torch.no_grad():
            for val_data in val_loader:
                inputs_val, labels_val = val_data[0], torch.as_tensor(val_data[1]).long()
                inputs_val = torch.transpose(inputs_val, 1, 3).double()
                
                
                outputs_val = net(inputs_val)
                
                _v, predicted_v = torch.max(outputs_val.data, 1)
                total_val += labels_val.size(0)
                correct_val += (predicted_v == labels_val).sum().item()
                val_loss = criterion(outputs_val, labels_val)
                running_val_loss += val_loss.item()
                
        #calculate average loss to get comparable values for test and train 
        running_loss/=len(train_loader)
        running_val_loss/=len(val_loader)
        train_acc=100*correct/total
        val_acc=100*correct_val/total_val
        
        
            
       
        writer.add_scalars('acc', {'train': train_acc,
                                    'val': val_acc}, epoch)
        writer.add_scalars('loss', {'train': running_loss,
                                'val': running_val_loss}, epoch)
    
        train_losses.append(running_loss)
        val_losses.append(running_val_loss)
        train_accs.append(train_acc)
        val_accs.append(val_acc)
        
        print(f'Epoch {epoch} Train {running_loss:.2f}|{train_acc:.2f}% Val {running_val_loss:.2f}|{val_acc:.2f}%')

    torch.save(net.state_dict(), output_path+'model.pth')

    create_training_plots(output_path, train_losses, val_losses, train_accs, val_accs)
    

    pd.DataFrame(data=[
                    ['train_accuracy', train_acc],
                    ['validation_accuracy', val_acc]
                    ], 
                 columns=['parameter', 'value']
                 ).to_csv(
                         output_path + 'summary.csv',
                         index = False, 
                         header = False,
                         sep = '\t',
                         mode = 'a'
                         )
    
    return 


def test_network(config,
                 net,
                 dataloader,
                 missing_values,
                 output_path,
#                 switch_dimensions,
                 classes):
    
    #make a final prediction over a trained network
    net = net.double() 
    net.load_state_dict(torch.load(output_path+'model.pth'))

    correct = 0
    total = 0
    class_correct = list(0. for i in range(len(classes)))
    class_total = list(0. for i in range(len(classes)))
    all_outputs = []
    all_labels = []
    with torch.no_grad():
        for data in dataloader: 
            inputs, labels = data[0], torch.as_tensor(data[1]).long()
#            print(inputs.shape)
            inputs = torch.transpose(inputs, 1, 3).double()
#            print(inputs.shape)
            outputs = net(inputs)
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()
            c = (predicted == labels).squeeze()
            for i in range(min(config['batch'],len(data))):
                label = labels[i]
                try:
                    class_correct[label] += c[i].item()
                    class_total[label] += 1
                except IndexError:
                    print('IndexError in class accuracy calculation')
                    pass    
        
            all_labels.append(labels)
            all_outputs.append(predicted)
            
    class_acc = np.array(class_correct)*100 / np.array(class_total)
             
    
    test_acc = 100*correct/total


        
    summary = [
            ['test_accuracy', test_acc]
            ]
    
    summary.extend(list(zip(classes, class_acc)
                        )
                    )
                
    summary.append(['network', net._modules]
                    )
    
    pd.DataFrame(data=summary, 
                 columns=['parameter', 'value']
                 ).to_csv(
                         output_path + 'summary.csv',
                         index = False, 
                         header = False,
                         sep = '\t',
                         mode = 'a'
                         )
        
        
    return test_acc
