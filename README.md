### Baseline papers
Set Functions for Time Series [(Horn et al. 2020)](http://proceedings.mlr.press/v119/horn20a/horn20a.pdf)<br>
Multi-Time Attention Networks for Irregularly Sampled Time Series [(Shukla \& Marlin 2021)](https://arxiv.org/pdf/2101.10318.pdf) <br>
Modeling Missing Data in Clinical Time Series with RNNs [(Lipton et al. 2016)](http://proceedings.mlr.press/v56/Lipton16.pdf)<br>
Recurrent Neural Networks for Multivariate Time Series with Missing Values [(Che et al. 2017)](https://www.nature.com/articles/s41598-018-24271-9.pdf) + [supplementary](https://static-content.springer.com/esm/art%3A10.1038%2Fs41598-018-24271-9/MediaObjects/41598_2018_24271_MOESM1_ESM.pdf)

### Datasets
[PAMAP2 Physical Activity Monitoring Data Set](https://archive.ics.uci.edu/ml/datasets/pamap2+physical+activity+monitoring)  <br>
[Localization Data for Person Activity Data Set](https://archive.ics.uci.edu/ml/datasets/Localization+Data+for+Person+Activity)<br>
[Gesture Phase Segmentation Data Set](https://archive.ics.uci.edu/ml/datasets/gesture+phase+segmentation)<br>

Notes on preprocessing: [link](https://docs.google.com/document/d/10HMfZpTvtB1zHetXmzZOIhvF2ERY6TXDGRW2g3TQLYk/edit?usp=sharing)<br>

### Tuning
[Notes](https://docs.google.com/document/d/1i9BARQr7jHGDKP9SUcasS4fXRfteg3a4jetoP_tl8_4/edit?usp=sharing)