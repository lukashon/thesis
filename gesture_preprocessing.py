# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import json
import glob
import os

from preprocessing_utils import sliding_window, adjust_names, create_train_test_split, standardize_dataset



def read_and_clean_up(file, 
                      output_directory, 
                      labels_encoding,
                      window_length=256, 
                      sliding_step = 8, 
                      standardize = True):
    
    df=pd.read_csv(file)
    df.sort_values('timestamp', inplace=True)
    df.drop('timestamp', inplace = True, axis=1)
    df['phase'] = df['phase'].replace('Preparação', 'Preparation')
    df['phase'] = [int(labels_encoding[ph]) for ph in df['phase']]
   
    st=''
    if standardize:
        df=standardize_dataset(df, 'phase')
        st = 'st_'
    
    idx = 0
    labels = []
    for phase, filtered in df.groupby('phase'):
        filtered=filtered.drop(columns='phase')
        subset, m=sliding_window(filtered, window_length, sliding_step)#extract m subsequences
        print(m)
        subset=subset.values.reshape(m,window_length,df.shape[1]-1)
        for i in range(m):
            pd.DataFrame(data=subset[i,:,:]).to_csv(output_directory+f'{window_length}_{st}_{phase}_{idx}.csv',
                                                    index = False)
            idx+=1
            labels.append(phase)
       
    print(f"{file} ready")
    
    return idx, labels

def extract_subsequences():
    
  
    output_directory = f'../datasets/Gesture/sequences/step_{sliding_step}_w_{window_size}/'
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)
    
    
    for file in files_raw:
        idx, labels = read_and_clean_up(file, 
                      output_directory, 
                      labels_encoding,
                      window_length=window_size, 
                      sliding_step = sliding_step, 
                      standardize = True)
        
    pd.DataFrame(data=labels).to_csv(output_directory+f'{window_size}_labels', index = False)
    print(f'Sliding step {sliding_step} done')
            
    return output_directory, labels



if __name__ == "__main__":
    
 
    files_raw = glob.glob('../datasets/Gesture/*raw.csv')
    files_va3 = glob.glob('../datasets/Gesture/*va3.csv')
         
    config = json.load(open('./config/gesture_config.json'))
    sliding_step = config['sliding_steps']
    standardize = config['standardize']
    window_size = config['window_size']
    test_share  = config['test_share']
    validation_share = config['validation_share']
    labels_encoding = config['labels_encoding']
   
    extract_subsequences()
    output_directory, labels = extract_subsequences()
    
    split = create_train_test_split(labels, test_share, validation_share)
    adjust_names(output_directory, split)
