# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd

import tensorflow as tf
from keras.layers import Input, Conv2D, Conv1D, BatchNormalization, ELU, Dense, Reshape, concatenate
from keras.layers import MaxPooling2D, MaxPooling1D, GlobalAveragePooling2D, GlobalAveragePooling1D, Permute
import keras
from tensorflow.keras import backend
from tensorflow.keras.models import Sequential

import glob
from models.lsst_disjoint import build_model, lsst_model
from sklearn.model_selection import train_test_split

from utils.preproc import add_missing_values


PATH = '/home/hanna/Documents/Thesis/datasets/LSST/'
train = glob.glob(PATH + 'LSSTDimension*_TRAIN.arff')
test = glob.glob(PATH + 'LSSTDimension*_TEST.arff')

columns = [f't{t}' for t in range(1, 37)]
columns.append('class')
train_data = [pd.read_csv(file, skiprows=41, names=columns) for file in train]
test_data = [pd.read_csv(file, skiprows=41, names=columns) for file in test]


def code_to_ranks(labels):
    labels = list(set(labels))
    labels.sort()
    
    encoding = {key: value for value, key in enumerate(labels)}
    
    return encoding

def standard_scaling(data_3dim):
    #Observations, attributes, TS  length
    scaled = (data_3dim - np.mean(data_3dim, axis=2, keepdims=True)) /  \
                                np.std(data_3dim, axis=2, keepdims=True)
    
    return scaled


train = np.stack(train_data, axis=2)
train = np.swapaxes(train, 1,2)
train_labels = train[:,1,-1]
train = train[:,:,:-1]
train = standard_scaling(train)

train_with_miss = [add_missing_values(obs) for obs in train]
train_with_miss = [np.transpose(np.stack((v, m)), axes=(1,2,0)) for v, m in train_with_miss]
train_with_miss = np.array(train_with_miss)
train_with_miss_no_ind = [np.stack((v)) for v, m in train_with_miss]
train_with_miss_no_ind = np.array(train_with_miss_no_ind)

enc = code_to_ranks(train_labels)
train_labels = np.array([enc[l] for l in train_labels]).reshape(-1,1)
train_labels = train_labels.astype(int)

test = np.stack(test_data, axis=2)
test = np.swapaxes(test, 1,2)
test_labels = test[:,1,-1]
test_labels = np.array([enc[l] for l in test_labels]).reshape(-1,1)
test_labels= test_labels.astype(int)
test = test[:,:,:-1]
test = standard_scaling(test)

test_with_miss = [add_missing_values(obs) for obs in test]
test_with_miss = [np.transpose(np.stack((v, m)), axes=(1,2,0)) for v, m in test_with_miss]
test_with_miss = np.array(test_with_miss) 
test_with_miss_no_ind = [np.stack((v)) for v, m in test_with_miss]
test_with_miss_no_ind = np.array(test_with_miss_no_ind)

#%%
X_train, X_val, y_train, y_val = train_test_split(train_with_miss, train_labels, test_size=0.1, random_state=42)

from models.lsst_disjoint import lsst_model
#model = build_model(input_shape = (6,36,1), nb_classes = 14)
model = lsst_model(input_shape = (6,36,2), num_classes = 14)



loss = tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True)

lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(
    initial_learning_rate=0.01,
    decay_steps=10000,
    decay_rate=0.9)

model.compile(optimizer=tf.keras.optimizers.Adam(
    learning_rate=lr_schedule),
              loss=loss,
              metrics=['accuracy'])

reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=50, min_lr=0.0001)

loss_hist = model.fit(X_train, y_train, epochs=500,
                       batch_size=16,
                       validation_data=(X_val, y_val),
                       callbacks=[reduce_lr,
                                  tf.keras.callbacks.EarlyStopping(monitor='val_loss',
                                                mode='min',
                                                verbose=0,
                                                patience=100,
                                                restore_best_weights=True)
                                  ]
                       )


import matplotlib.pyplot as plt

plt.plot(loss_hist.history['loss'])
plt.plot(loss_hist.history['val_loss'])

model.evaluate(test_with_miss,  test_labels, verbose=2)
