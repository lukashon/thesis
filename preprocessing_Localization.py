# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import glob
import os
import json
from preprocessing_utils import sliding_window



    
def extract_subsequences():
    
    for step in sliding_steps:
        
        output_directory = f'../datasets/Localization/sequences/step_{step}/'
        if not os.path.exists(output_directory):
            os.makedirs(output_directory)
        
      
        labels = []
        idx = 0
        
        for name, df in data.groupby(['sequence_name']):
            st=''
            if standardize:
                clean=df[['x_coord', 'y_coord','z_coord']]
                stdev=np.nanstd(clean, axis=0)
                mean=np.nanmean(clean, axis=0)
                clean=(clean-mean)/stdev 
                clean['activity']=df['activity']
                df = clean
                st = 'st_'
                
            for act, adf in df.groupby(['activity']):
                
    
                adf=adf.drop(columns='activity')
                if len(adf) < window_size:
                    print(f'{act} in subj {name} skipped as number of observations {len(adf)} less that window size {window_size}')
                    
                else:
                    subset, m=sliding_window(adf, window_size, step)#extract m subsequences
                    print(act, m, subset.shape)
                    subset=subset.values.reshape(m,window_size,adf.shape[1])
                    for i in range(m):
                        pd.DataFrame(data=subset[i,:,:]).to_csv(output_directory+f'{window_size}_{st}act{act}_{idx}.csv',
                                                                index = False)
                        idx+=1
                        labels.append(act)
                       
            print(f"{name} ready")
 
 
        pd.DataFrame(data=labels).to_csv(output_directory+f'{window_size}_labels', index = False)
        print(f'Sliding step {step} done')
            
    return output_directory, labels

if __name__ == "__main__":
    
    columns = pd.read_csv('./config/Localization_columns')['column_name']

    
    config = json.load(open('./config/Localization_config.json'))
    sliding_steps = config['sliding_steps']
    standardize = config['standardize']
    window_size = config['window_size']
    
    data = pd.read_csv('../datasets/Localization/ConfLongDemo_JSI.txt', names = columns)
    data['tag'] = [config['tags'][t] for t in data['tag']]
    data = data[data.tag == 'ankle_left' ]
    data.sort_values('timestamp', inplace = True)
    
    output_directory, labels = extract_subsequences()

