# -*- coding: utf-8 -*-
import torch.nn as nn
import torch.nn.functional as F
import torch

from torchsummary import summary

'''torch.nn.Conv2d(in_channels, 
                    out_channels, 
                    kernel_size,
                    stride=1, padding=0, dilation=1, groups=1, 
                    bias=True, padding_mode='zeros', device=None, dtype=None)
'''


class RotationNetPamap2(nn.Module):
    def __init__(self, n_channels = 2):
        super().__init__()
        self.conv1 = nn.Conv2d(n_channels, 5, (1, 4))
        self.batchnorm1 = nn.BatchNorm2d(5)
        self.pool = nn.MaxPool2d(2, 2)
        self.conv2 = nn.Conv2d(30, 10, (1, 4))
        self.batchnorm2 = nn.BatchNorm2d(10)
        self.conv3 = nn.Conv2d(1, 5, (1, 4))
        
        self.fc1 = nn.Linear(5*10*61, 16)
#        self.fc2 = nn.Linear(64, 16)
        self.fc3 = nn.Linear(16, 8)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = self.batchnorm1(x)
        x = x.permute(0,2,1,3)
        x = F.relu(self.conv2(x))
        x = self.pool(x)
        x = self.batchnorm2(x)
        x = x.reshape((x.shape[0], 1, x.shape[1]*x.shape[2], x.shape[3]))
        x = F.relu(self.conv3(x))
        x = self.pool(x)
#        x = self.pool(F.relu(self.conv2(x)))
        x = torch.flatten(x, 1) # flatten all dimensions except batch
        x = F.relu(self.fc1(x))
#        x = F.relu(self.fc2(x))
        x = self.fc3(x)
        return x
    

summary(RotationNetPamap2(), (2, 30, 256), 8)
