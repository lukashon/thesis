import numpy as np
import pandas as pd
import torch
from torch.utils.data import DataLoader, Dataset
import torch.nn as nn
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
import time


channels = 1 
class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()
        self.conv1 = nn.Conv1d(channels, channels*8, 5)
        self.pool = nn.MaxPool1d(2, 2)
        self.conv2 = nn.Conv1d(channels*8, channels*4, 5)
        self.fc1 = nn.Linear(channels*4*61, 732)
        self.fc2 = nn.Linear(732, 4)

    def forward(self, x):
        x = self.pool(torch.sigmoid(self.conv1(x)))
        x =self.pool(torch.sigmoid(self.conv2(x)))
        x = x.view(-1, channels*4*61)
        x = torch.sigmoid(self.fc1(x))
        x = self.fc2(x)
        return x
    