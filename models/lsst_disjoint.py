# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd

import tensorflow as tf

from tensorflow.keras import backend
from tensorflow.keras.models import Sequential


from tensorflow import keras


from tensorflow.keras.layers import Dense, Input, Conv2D, Conv1D, BatchNormalization, ELU, Reshape, concatenate, ReLU
from tensorflow.keras.layers import Dropout, MaxPooling2D, MaxPooling1D, GlobalAveragePooling2D, GlobalAveragePooling1D, Permute


def build_model(input_shape, nb_classes):

    X_input = Input(shape=input_shape)

 	# Temporal Convolutions
    conv1 = Conv2D(64, (6, 1), strides=1, padding="same", kernel_initializer='he_uniform')(X_input)
    conv1 = BatchNormalization()(conv1)
    conv1 = ELU(alpha=1.0)(conv1)
    # Spatial Convolutions
    conv1 = Conv2D(64, (1, input_shape[1]), strides=1, padding="valid", kernel_initializer='he_uniform')(conv1)
    conv1 = BatchNormalization()(conv1)
    conv1 = ELU(alpha=1.0)(conv1)
    conv1 = Permute((1,3,2))(conv1)

    # Temporal Convolutions
    conv2 = Conv2D(64, (5, 1), strides=1, padding="same", kernel_initializer='he_uniform')(conv1)
    conv2 = BatchNormalization()(conv2)
    conv2 = ELU(alpha=1.0)(conv2)
    # Spatial Convolutions
    conv2 = Conv2D(64, (1, conv2.shape[2]), strides=1, padding="valid", kernel_initializer='he_uniform')(conv2)
    conv2 = BatchNormalization()(conv2)
    conv2 = ELU(alpha=1.0)(conv2)
    conv2 = Permute((1, 3, 2))(conv2)

    # Temporal Convolutions
    conv3 = Conv2D(64, (5, 1), strides=1, padding="same", kernel_initializer='he_uniform')(conv2)
    conv3 = BatchNormalization()(conv3)
    conv3 = ELU(alpha=1.0)(conv3)
    # Spatial Convolutions
    conv3 = Conv2D(64, (1, conv3.shape[2]), strides=1, padding="valid", kernel_initializer='he_uniform')(conv2)
    conv3 = BatchNormalization()(conv3)
    conv3 = ELU(alpha=1.0)(conv3)
    conv3 = Permute((1, 3, 2))(conv3)

    # Temporal Convolutions
    conv4 = Conv2D(64, (3, 1), strides=1, padding="same", kernel_initializer='he_uniform')(conv3)
    conv4 = BatchNormalization()(conv4)
    conv4 = ELU(alpha=1.0)(conv4)
    # Spatial Convolutions
    conv4 = Conv2D(64, (1, conv4.shape[2]), strides=1, padding="valid", kernel_initializer='he_uniform')(conv4)
    conv4 = BatchNormalization()(conv4)
    conv4 = ELU(alpha=1.0)(conv4)

    MaxPool = MaxPooling2D(pool_size=(5, 1), strides=None, padding='valid')(conv1)
    gap_DCNN = GlobalAveragePooling2D()(MaxPool)

    dense = Dense(128, activation="relu")(gap_DCNN)
    output_layer = keras.layers.Dense(nb_classes)(dense)
    model = keras.models.Model(inputs=X_input, outputs=output_layer)
    
    return model
#%%

def lsst_model(input_shape, num_classes):
    
    X_input = Input(shape=input_shape)

 	# Temporal Convolutions
    conv1 = Conv2D(64, (1, 1), strides=1, padding="same", kernel_initializer='he_uniform')(X_input)
    conv1 = BatchNormalization()(conv1)
#    conv1 = ReLU(alpha=1.0)(conv1)
    conv1 = ReLU()(conv1)
    conv1 = Permute((3,2,1))(conv1)
    
    conv1 = Conv2D(32, (1, 3), strides=1, padding="same", kernel_initializer='he_uniform')(conv1)
    conv1 = BatchNormalization()(conv1)
#    conv1 = ReLU(alpha=1.0)(conv1)
    conv1 = ReLU()(conv1)
    conv1 = Permute((3,2,1))(conv1)
    
    conv2 = Conv2D(64, (1, 1), strides=1, padding="same", kernel_initializer='he_uniform')(conv1)
    conv2 = BatchNormalization()(conv2)
#    conv2 = ReLU(alpha=1.0)(conv2)
    conv2 = ReLU()(conv2)
    conv2 = Permute((3,2,1))(conv2)
    
    conv2 = Conv2D(32, (1, 3), strides=1, padding="same", kernel_initializer='he_uniform')(conv2)
    conv2 = BatchNormalization()(conv2)
#    conv2 = ReLU(alpha=1.0)(conv2)
    conv2 = ReLU()(conv2)

    
    
    
    
    MaxPool = MaxPooling2D(pool_size=(5, 1), strides=None, padding='same')(conv2)
    gap_DCNN = GlobalAveragePooling2D()(MaxPool)
    
    output_layer = tf.keras.layers.Dense(num_classes)(gap_DCNN)
    model = tf.keras.models.Model(inputs=X_input, outputs=output_layer)
    
    
    return model
    
    
m = lsst_model((6,36,2), 14)
m.summary()
