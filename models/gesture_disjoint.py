# -*- coding: utf-8 -*-
import torch.nn as nn
import torch.nn.functional as F
import torch

from torchsummary import summary

'''torch.nn.Conv2d(in_channels, 
                    out_channels, 
                    kernel_size,
                    stride=1, padding=0, dilation=1, groups=1, 
                    bias=True, padding_mode='zeros', device=None, dtype=None)
'''


class DisjointNet(nn.Module):
    def __init__(self, n_channels = 2):
        super().__init__()
        
        self.bn = nn.BatchNorm2d(64)
        #Temporal
        self.conv1t = nn.Conv2d(n_channels, 64, (8, 1), padding="same") # t*1
        #Spatial
        self.conv1s = nn.Conv2d(64, 64, (1, 18), padding='valid') # M*d
    

        
        #Temporal
        self.conv2t = nn.Conv2d(3, 64, (5, 1), padding="same")
        #Spatial
        self.conv2s = nn.Conv2d(64, 64, (1, 18), padding='valid')
        
        #Temporal
        self.conv3t = nn.Conv2d(47, 64, (5,1), padding="same")
        #Spatial
        self.conv3s = nn.Conv2d(64, 64, (1, 18), padding='valid')
        
        #Temporal
        self.conv4t = nn.Conv2d(47, 64, (3,1), padding="same")
        #Spatial
        self.conv4s = nn.Conv2d(64, 64, (1, 18), padding='valid')
        
        
        self.pool = nn.MaxPool2d(5, 1)
        
        self.fc = nn.Linear(47, 5)

    def forward(self, x):
        
        #Conv1
        x = self.conv1t(x)
        x = self.bn(x)
        x = F.elu(x, alpha=0.1)
        
        x = self.conv1s(x)
        x = self.bn(x)
        x = F.elu(x, alpha=0.1)
        x = x.permute(0,3,2,1)
        
        #Conv2
        x = self.conv2t(x)
        x = self.bn(x)
        x = F.elu(x, alpha=0.1)
        
        x = self.conv2s(x)
        x = self.bn(x)
        x = F.elu(x, alpha=0.1)
        x = x.permute(0,3,2,1)
        
        #Conv3
        x = self.conv3t(x)
        x = self.bn(x)
        x = F.elu(x, alpha=0.1)
        
        x = self.conv3s(x)
        x = self.bn(x)
        x = F.elu(x, alpha=0.1)
        x = x.permute(0,3,2,1)
        
        #Conv4
        x = self.conv4t(x)
        x = self.bn(x)
        x = F.elu(x, alpha=0.1)
        
        x = self.conv4s(x)
        x = self.bn(x)
        x = F.elu(x, alpha=0.1)
        x = x.permute(0,3,2,1)
        
        x = self.pool(x)
        
        #Global Aveage Pooling
        x = x.mean([2, 3])
        
        x = self.fc(x)
        
        return x
    

summary(DisjointNet(), (2, 18, 20), 8)

"""
Default order of dimensions in Keras: (batch_size, height, width, channels)
Default order of dimensions in Pytorch: (batch_size, channels, height, width)  
Original implementation

    def build_model(self, input_shape, nb_classes):

        X_input = Input(shape=input_shape)

 	# Temporal Convolutions
        conv1 = Conv2D(64, (8, 1), strides=1, padding="same", kernel_initializer='he_uniform')(X_input)
        conv1 = BatchNormalization()(conv1)
        conv1 = ELU(alpha=1.0)(conv1)
        # Spatial Convolutions
        conv1 = Conv2D(64, (1, input_shape[1]), strides=1, padding="valid", kernel_initializer='he_uniform')(conv1)
        conv1 = BatchNormalization()(conv1)
        conv1 = ELU(alpha=1.0)(conv1)
        conv1 = Permute((1,3,2))(conv1)

        # Temporal Convolutions
        conv2 = Conv2D(64, (5, 1), strides=1, padding="same", kernel_initializer='he_uniform')(conv1)
        conv2 = BatchNormalization()(conv2)
        conv2 = ELU(alpha=1.0)(conv2)
        # Spatial Convolutions
        conv2 = Conv2D(64, (1, conv2.shape[2]), strides=1, padding="valid", kernel_initializer='he_uniform')(conv2)
        conv2 = BatchNormalization()(conv2)
        conv2 = ELU(alpha=1.0)(conv2)
        conv2 = Permute((1, 3, 2))(conv2)

        # Temporal Convolutions
        conv3 = Conv2D(64, (5, 1), strides=1, padding="same", kernel_initializer='he_uniform')(conv2)
        conv3 = BatchNormalization()(conv3)
        conv3 = ELU(alpha=1.0)(conv3)
        # Spatial Convolutions
        conv3 = Conv2D(64, (1, conv3.shape[2]), strides=1, padding="valid", kernel_initializer='he_uniform')(conv2)
        conv3 = BatchNormalization()(conv3)
        conv3 = ELU(alpha=1.0)(conv3)
        conv3 = Permute((1, 3, 2))(conv3)

        # Temporal Convolutions
        conv4 = Conv2D(64, (3, 1), strides=1, padding="same", kernel_initializer='he_uniform')(conv3)
        conv4 = BatchNormalization()(conv4)
        conv4 = ELU(alpha=1.0)(conv4)
        # Spatial Convolutions
        conv4 = Conv2D(64, (1, conv4.shape[2]), strides=1, padding="valid", kernel_initializer='he_uniform')(conv4)
        conv4 = BatchNormalization()(conv4)
        conv4 = ELU(alpha=1.0)(conv4)

        MaxPool = MaxPooling2D(pool_size=(5, 1), strides=None, padding='valid')(conv4)
        gap_DCNN = GlobalAveragePooling2D()(MaxPool)

        dense = Dense(128, activation="relu")(gap_DCNN)
        output_layer = keras.layers.Dense(nb_classes, activation='softmax')(dense)
        model = keras.models.Model(inputs=X_input, outputs=output_layer)
        return model
        
"""