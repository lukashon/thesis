# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
import random
import glob
import os
import json
from utils.preproc import sliding_window, create_train_test_split, standardize_dataset



def read_and_clean_up(file, 
                      output_directory, 
                      labels,
                      idx,
                      window_length=256, 
                      sliding_step = 8, 
                      standardize = True):
    
    df=pd.read_csv(file, delim_whitespace=True, names = columns)
    df.drop(columns=irrelevant_columns, inplace=True)
    df=df[df['activityID'].isin(relevant_activities)]
    
    st=''
    if standardize:
        df = standardize_dataset(df, 'activityID')
        st = 'st_'
    
    for act in relevant_activities:
        filtered=df[df['activityID']==act]
        filtered=filtered.drop(columns='activityID')
        subset, m=sliding_window(filtered, window_length, sliding_step)#extract m subsequences
        print(m)
        subset=subset.values.reshape(m,window_length,df.shape[1]-1)
        for i in range(m):
            pd.DataFrame(data=subset[i,:,:]).to_csv(output_directory+f'{window_length}_{st}act_{act}_{idx}.csv',
                                                    index = False)
            idx+=1
            labels.append(act)
       
    print(f"{file} ready")
    
    return idx, labels


def extract_subsequences():
    
    for step in sliding_steps:
        
        output_directory = f'../datasets/PAMAP2_Dataset/sequences/step_{step}/'
        if not os.path.exists(output_directory):
            os.makedirs(output_directory)
        
        idx = 0
        labels = []
        
        for file in files:
            idx, labels = read_and_clean_up(file, 
                          output_directory, 
                          labels,
                          idx,
                          window_length=window_size, 
                          sliding_step = step, 
                          standardize = True)
            
        pd.DataFrame(data=labels).to_csv(output_directory+f'{window_size}_labels.txt', index = False)
        print(f'Sliding step {step} done')
            
    return output_directory, labels




if __name__ == "__main__":
    
    files = glob.glob('../datasets/PAMAP2_Dataset/Protocol/*')
    files = list(filter(lambda f: ('108' not in f) and ('109' not in f), files))
    
    columns = pd.read_csv('./config/pamap2_columns')['column_name'].to_list()
    irrelevant_columns = pd.read_csv('./config/pamap2_irrelevant_columns')['column_name'].to_list()
    
    config = json.load(open('./config/pamap2_config.json'))
    
    relevant_activities = config['relevant_activities']
    sliding_steps = config['sliding_steps']
    window_size= config['window_size']
#    test_share = config['test_share']
#    validation_share = config['validation_share']

    output_directory, labels = extract_subsequences()
    
#    split = create_train_test_split(labels, test_share, validation_share)
#    adjust_names(output_directory, split)

