import numpy as np
import pandas as pd
import json
from datetime import datetime
import os

from utils.dataloader import create_dataloaders
from utils.training import train_network, test_network


from models.pamap2_Rotation import RotationNetPamap2


np.random.seed(8)


def grid_search(config):
    
    for stack_missing_indicator in  config['stack_missing_indicator']:
    
        if stack_missing_indicator:
            st_ind = 'with_ind'
            n_channels = 2

            
        else:
            st_ind = 'no_ind'
            n_channels = 1

        
        for sparcity in config['miss_sparcity']:
            for fill_method in config['mis_fill_method']:
                
                #create folder to store training summary and tensor logs
                ts = datetime.now().strftime("%Y%m%d_%H%M%S")
                output_path = './runs/pamap2/' + \
                        config['data_dir'].split('/')[-2] + \
                        f'/{ts}_sp_{sparcity}_fill_{fill_method}_{st_ind}/'
                
                if not os.path.exists(output_path):
                    os.makedirs(output_path)
                    
                    
          
                pd.DataFrame(data=[
                            ['epochs', config['epochs']],
                            ['sparcity', sparcity],
                            ['fill_method', fill_method],
                            ['indicator_added', stack_missing_indicator]
                            ], 
                         columns=['parameter', 'value']
                         ).to_csv(
                                 output_path + 'summary.csv',
                                 index = False, 
                                 sep = '\t'
                                 )
        
                
            
                trainloader, valloader, testloader = create_dataloaders(config,
                                                            add_missing_indicator=stack_missing_indicator,
                                                            sparcity=sparcity, 
                                                            fill_method=fill_method,
                                                            recode=True
                                                            )
                print('Dataloaders created, training...')
                
                train_network(config, 
                      net  = RotationNetPamap2(n_channels=n_channels), 
                      train_loader = trainloader,
                      val_loader = valloader, 
                      output_path = output_path,
                      missing_values = True,
    #                  switch_dimensions = switch_dimensions,
                      classes = classes)
                
                test_acc = test_network(config,
                             net= RotationNetPamap2(n_channels=n_channels),
                             dataloader = testloader,
                             missing_values = True,
                             output_path = output_path,
    #                         switch_dimensions = switch_dimensions,
                             classes = classes)
                print(f'Sp {sparcity} fill {fill_method}: test acc {test_acc}')
                
            
            
if __name__ == "__main__":
    

    config = json.load(open('./config_runs/pamap2_run.json'))
    config_prep = json.load(open('./config/pamap2_config.json'))
    classes = list(config_prep['labels_encoding'].keys())
    classes.sort()
    

    grid_search(config)
