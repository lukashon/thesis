# -*- coding: utf-8 -*-
import pandas as pd
import glob
import numpy as np

PATH = '/home/hanna/Documents/Thesis/datasets/LSST/'
train = glob.glob(PATH + 'LSSTDimension*_TRAIN.arff')
test = glob.glob(PATH + 'LSSTDimension*_TEST.arff')

columns = [f't{t}' for t in range(1,37)]
columns.append('class')
train_data = [pd.read_csv(file,  skiprows=41, names = columns) for file in train]
test_data = [pd.read_csv(file,  skiprows=41, names = columns) for file in test]



lsst = pd.read_csv('/home/hanna/Documents/Thesis/datasets/LSST/LSSTDimension1_TEST.arff', \
                                                       skiprows=44)

lsst2 = pd.read_csv('/home/hanna/Documents/Thesis/datasets/LSST/LSSTDimension2_TEST.arff', \
                                                       skiprows=44)

train = np.stack(train_data, axis=2)
train = np.swapaxes(train, 1,2)
train_labels = train[:,1,-1]
train = train[:,:,:-1].reshape(-1,1)

test = np.stack(test_data, axis=2)
test = np.swapaxes(test, 1,2)
test_labels = test[:,1,-1]
test = test[:,:,:-1].reshape(-1,1)
